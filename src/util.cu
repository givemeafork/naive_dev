#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <stdio.h>
#include <iostream>

#include <time.h>


/* user defines */

// point to blur around
#define BlurCentralPointX 200
#define BlurCentralPointY 200

// number of streams to work with
#define max_stream_number 4

// B L U R config //
#define BlurPixelDiameter 16 // Should be Even number - min 5
#define SizeOfBlur 30  //percentage of width that is CLEAR (no Blur) around BlurCentralPoint
#define BlurAgression 80 // ot 1 do 100; (Bigger than 0)

double cpu_time_used;


/* user defined structures */
struct point_map
{
        int X;
        int Y;
}point_map;

/* global variables */
static double* d_matrix;
static uchar* d_original_image;
static uchar* d_output_image;

/* functions */

__global__ void image_processing_blur(uchar *original_image, uchar *output_image, const int img_lines, const int img_cols, double* d_matrix, const int start_line, const int stop_line);

void cuda_transfer_image_segment(uchar* h_image, int offset_in, int offset_out, int rows, int cols, cudaStream_t stream_nr, int host2dev)
{
	int offsetIn = 0;
	int offsetOut = 0;

	if(host2dev)
	{
		for(int i=0; i<rows; i++)
		{
			offsetIn = offset_in + i*cols*sizeof(uchar);
			offsetOut = offset_out + i*(cols+32*2)*sizeof(uchar);
			
			// copy left side border of row
			cudaMemcpyAsync(d_original_image + offsetOut, h_image + offsetIn, 32 * sizeof(uchar), cudaMemcpyHostToDevice, stream_nr);
			// copy image row
			cudaMemcpyAsync(d_original_image + offsetOut + 32*sizeof(uchar), h_image + offsetIn, cols*sizeof(uchar), cudaMemcpyHostToDevice, stream_nr);
			// copy right side border of row
			cudaMemcpyAsync(d_original_image + offsetOut + 32*sizeof(uchar) + cols*sizeof(uchar), 
									h_image + offsetIn + (cols-32)*sizeof(uchar), 
												32*sizeof(uchar), cudaMemcpyHostToDevice, stream_nr);
		}
	}
	else{
		cudaMemcpyAsync(h_image + offset_out, d_output_image + offset_in, rows*cols*sizeof(uchar), cudaMemcpyDeviceToHost, stream_nr);
	}

}

cudaError_t transfer_image_to_device(uchar* h_original_image, uchar* h_output_image, int rows, int cols)
{
	clock_t start, end;
	int index_dep = rows*cols / max_stream_number;
	int index_dep_device = rows*(cols+32*2) / max_stream_number;
	int size_of_shared_mem = (32*3)*(32*3) * sizeof(uchar);
	//printf("Shared memory : %d\n", size_of_shared_mem);


	cudaStream_t streams[max_stream_number];
	for(int i=0; i< max_stream_number; i++)
	{
		cudaStreamCreate(streams +i);
	}
	
	start = clock();

	dim3 dimBlock(32, 32); 
	dim3 dimGrid(rows / 32 + 1, cols / 32 + 1); 
	cudaError_t cudaStatus;
	// Choose which GPU to run on, change this on a multi-GPU system.
	cudaStatus = cudaSetDevice(0);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?");
		goto Error;

	}

	cudaStatus = cudaMalloc((void**)&d_original_image, (rows+2*32) * (cols+2*32) * sizeof(uchar));
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
		goto Error;
	}

	cudaStatus = cudaMalloc((void**)&d_output_image, rows * cols * sizeof(uchar));
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
		goto Error;
	}
	
	// transfer the upper border and the down one
	cuda_transfer_image_segment(h_original_image, 0, 0, 32, cols, streams[0], 1);
	cuda_transfer_image_segment(h_original_image, rows*cols, rows*cols, 32, cols, streams[0], 1);
	// wait for it to be transferred .. just to be sure
	cudaStreamSynchronize(streams[0]);

	for(int i=0; i< max_stream_number; i++){
		
		// Copy input vectors from host memory to GPU buffers.
		cuda_transfer_image_segment(h_original_image, index_dep*i, 32*(cols+32*2) + index_dep_device*i, rows/max_stream_number, cols, streams[i], 1);

		image_processing_blur <<< dimGrid, dimBlock, size_of_shared_mem, streams[i] >>>(d_original_image, d_output_image,rows ,cols, d_matrix, rows*i/max_stream_number, rows*(i+1)/max_stream_number); 

	}
	
	cudaStreamSynchronize(streams[0]);
	// Copy output vector from GPU buffer to host memory.
	cuda_transfer_image_segment(h_original_image, 0, 0, rows/max_stream_number-BlurPixelDiameter, cols, streams[0], 0);

	for(int i=1; i < max_stream_number;i++)
	{
		cudaStreamSynchronize(streams[i]);

		// Copy output vector from GPU buffer to host memory.
		cuda_transfer_image_segment(h_original_image, index_dep*i, index_dep*i, rows/max_stream_number-BlurPixelDiameter, cols, streams[i], 0);

		image_processing_blur <<<dimGrid, dimBlock, size_of_shared_mem, streams[i] >>>(d_original_image, d_output_image, rows, cols, d_matrix, rows*i/max_stream_number - 2*BlurPixelDiameter, rows*i/max_stream_number + 2* BlurPixelDiameter); 
	}

	cudaStreamSynchronize(streams[0]);
	for(int i=1; i< max_stream_number; i++)
	{
		cudaStreamSynchronize(streams[i]);
		// Copy what is left (the middle borders)

		cuda_transfer_image_segment(h_original_image, index_dep*i - cols*BlurPixelDiameter, index_dep*i - cols*BlurPixelDiameter,  2*BlurPixelDiameter, cols, streams[i], 0);

	}

	// cudaDeviceSynchronize waits for the kernel to finish, and returns
	// any errors encountered during the launch.
	cudaStatus = cudaDeviceSynchronize();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching addKernel!\n", cudaStatus);
		goto Error;
	}
	
	end = clock();
	
	cpu_time_used = ((double)(end - start)) / CLOCKS_PER_SEC;
	//printf("<*> Time used %f seconds!\n", cpu_time_used);

Error:
	for(int i=0; i< max_stream_number; i++)
	{
		cudaStreamDestroy(streams[i]);
	}
	cudaFree(d_original_image);
	cudaFree(d_output_image);

	return cudaStatus;
}

__global__ void image_processing_blur(uchar *original_image, uchar *output_image,const int img_lines, const int img_cols, double* d_matrix, const int start_line, const int stop_line)
{
	struct point_map thread_coords;
	
	thread_coords.X = blockDim.x*blockIdx.x + threadIdx.x + start_line ;
	thread_coords.Y = blockDim.y*blockIdx.y + threadIdx.y ;


	__shared__ uchar shared_original_image[(32*3)*(32*3)];
	if(thread_coords.X < stop_line && thread_coords.Y < img_cols){

		for( int i= -1; i<= 1;i++){
			for( int j=-1; j<= 1;j++){
				shared_original_image[(threadIdx.x + 32*(i+1))*32*3 + 32*(j+1)+ threadIdx.y] = original_image[(thread_coords.X+(i+1)*32)*(img_cols+32*2) +32*(j+1) + thread_coords.Y];
			}
		}
		
	__syncthreads();
		
		//output_image[thread_coords.X *img_cols + thread_coords.Y] = shared_original_image[(threadIdx.x +32)*32*3 + 32 + threadIdx.y];

		//output_image[thread_coords.X *img_cols + thread_coords.Y] = original_image[(thread_coords.X+32) *(img_cols+32*2) + 32 + thread_coords.Y];

	}


	if (thread_coords.X < stop_line && thread_coords.Y < img_cols)
	{
		// output_image[thread_coords.X*img_cols + thread_coords.Y] = original_image[thread_coords.X*img_cols + thread_coords.Y];
		int i, j, z, t; 
		
		double Number = 0;
		double Number2 = 0;

		double Distance;
		double DistanceRatio;
		double Sybiraemo = 0;
		
		double blurr_for_pixel;

		double MaxRatio;
		////////////////

		MaxRatio = (double)MAX(BlurCentralPointX - ((double)SizeOfBlur * img_cols / 100), img_cols - BlurCentralPointX + ((double)SizeOfBlur * img_cols / 100)) / ((double)SizeOfBlur * img_cols / 100);
		
		Distance = sqrt(pow(abs((double)BlurCentralPointY - thread_coords.Y), 2) + pow(abs((double)BlurCentralPointX - thread_coords.X), 2));
		if (Distance > ((double)SizeOfBlur * img_cols / 100))
		{
			//d_matrix[thread_coords.X * img_cols + thread_coords.Y] = 1;
			blurr_for_pixel = 1;
		}
		else
		{

			DistanceRatio = Distance / ((double)SizeOfBlur * img_cols / 100);
			blurr_for_pixel = 0.5 - ((double)BlurAgression/100 *( DistanceRatio/ MaxRatio));
			if(blurr_for_pixel < 0) blurr_for_pixel = 0;
		}

		if (thread_coords.X < stop_line)
		{
			if (thread_coords.Y < img_cols)
			{
				i = threadIdx.x+32;
				j = threadIdx.y+32;

				Sybiraemo = 0;

				Number2 = ((double)(blurr_for_pixel)/(pow((double)BlurPixelDiameter,2) -1 - (12 + (2*(BlurPixelDiameter - 5)))));
				
				for( z = -BlurPixelDiameter/2; z <= BlurPixelDiameter/2; z++)
				{
						for( t = -BlurPixelDiameter/2; t <= BlurPixelDiameter/2; t++)
						{
							Sybiraemo += shared_original_image[(i+z)*32*3 + j+t]/256.0;
						}
				}
					
				//Number2 *= Sybiraemo;
				//Number = (1-blurr_for_pixel)*shared_original_image[i*32*3 + j] + Number2;
				Number = Sybiraemo;
				if(Number > 255) 
					Number = 255;
				if(Number < 0) 
					Number = 0;

				output_image[thread_coords.X *img_cols + thread_coords.Y] =(int) Number;
			}
		}



	}
	

}
