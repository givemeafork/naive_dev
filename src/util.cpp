#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>

using namespace cv;
using namespace std;

int read_image(Mat *inputImageRGB, Mat *outputImageRGB, const cv::String& filename)
{
    *inputImageRGB = imread(filename, IMREAD_GRAYSCALE);   // Read the file

    //*outputImageRGB = inputImageRGB->clone();

    if(! (*inputImageRGB).data )                              // Check for invalid input
    {
        cout <<  "Could not open or find the image" << std::endl ;
        return -1;
    }

    //namedWindow( "Original image", WINDOW_NORMAL );// Create a window for display.
    //imshow( "Original image", *inputImageRGB );                   // Show our image inside it.

    return 0;
}
