#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <stdio.h>
#include <iostream>

#include <libgen.h>
#include <string.h>

using namespace cv;
using namespace std;

int read_image(Mat *inputImageRGB, Mat *outputImageRGB, const cv::String& filename);
cudaError_t transfer_image_to_device(uchar* h_original_image, uchar* h_output_image, int rows, int cols);
extern double cpu_time_used;


int main(int argc, char** argv)
{

	if( argc != 2)
    {
     cout <<" Usage: display_image ImageToLoadAndDisplay" << endl;
     return -1;
	}
	
	Mat input_helper;

	// read_image(&input_helper, nullptr, "./input/demo_image.jpeg");
	read_image(&input_helper, nullptr, argv[1]);

	Mat *input_image = nullptr;
	Mat *output_image = nullptr;
	cudaMallocHost((void**)&input_image, (sizeof(input_helper)));
	cudaMallocHost((void**)&output_image, sizeof(input_helper));

	memcpy(input_image, &input_helper, sizeof(input_helper));
	memcpy(output_image, &input_helper, sizeof(input_helper));

	transfer_image_to_device((*input_image).ptr<uchar>(0), (*output_image).ptr<uchar>(0), (*input_image).rows, (*input_image).cols);

	printf("%s:%3.6lf", basename(argv[1]), cpu_time_used);

	namedWindow("Processed image", WINDOW_NORMAL);// Create a window for display.
	imshow("Processed image", (Mat)(*output_image));                   // Show our image inside it.
	waitKey(0);

	return 0;
}
