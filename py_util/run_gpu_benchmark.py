import os
import sys
from matplotlib.ticker import FormatStrFormatter
from matplotlib.ticker import StrMethodFormatter
import matplotlib.pyplot as plt

img_set_path = r"./input"
img_folder = os.listdir(img_set_path)
arg_list = sys.argv

final_results = list()
img_label = list()
img_time = list()

# argv for python: name of program to run  example run_cmd
# run_cmd is called each time with a name of a file
# run_cmd should return something in this format
# result format file_name:time\n          example 1868_2802.jpg:3.92766\n
# extract the file name and the number


def extract_plot_info(l_data_string, p_img_label, p_img_time):
    for res in l_data_string:
        res.replace('\n', '')
        res_split = res.split(':')
        # file_name = res_split[0].split('.')
        file_name = res_split[0]
        time_value = res_split[1]
        p_img_label.append(file_name)
        p_img_time.append(float(time_value))
        print(p_img_time[-1])


def show_plot_info(p_img_label, p_img_time):
    # print(p_img_label)
    # print(p_img_time)
    fig, ax = plt.subplots(1)
    fig.suptitle('GPU Analysis')
    ax.set(xlabel='Image Resolution', ylabel='Time(s)')
    # ax.yaxis.set_major_formatter(FormatStrFormatter('%.5f'))
    # plt.gca().yaxis.set_major_formatter(StrMethodFormatter('{x:,.6f}')) # 2 decimal places
    
    plt.bar(p_img_label, p_img_time)
    plt.show()


if __name__ == "__main__":

    if len(arg_list) == 2:
        cmd_str = arg_list[1]
        for file in img_folder:
            sys_call = cmd_str + " " + img_set_path + r"/"+file
            print(sys_call)
            out_str = os.popen(sys_call).read()
            final_results.append(out_str)
        # print(final_results)
        extract_plot_info(final_results, img_label, img_time)
        show_plot_info(img_label, img_time)

    else:
        print("Write in command line the command to be executed without any additional arguments")


